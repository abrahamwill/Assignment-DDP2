import java.util.Random;
import java.util.ArrayList;

//Membuat class animal
public class Animal{
    private String name;
    private int bodylength;
    private boolean pet;

    //Constructor super
    public Animal(String name, int bodylength, boolean pet) {
        this.name = name;
        this.bodylength = bodylength;
        this.pet = pet;
    }
    //overide method

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBodylength() {
        return bodylength;
    }

    public void setBodylength(int bodylength) {
        this.bodylength = bodylength;
    }

    public boolean isPet() {
        return pet;
    }

}

//Class Cats
class Cats extends Animal{
    ArrayList<String> listnama = new ArrayList<>(); //untuk menyimpan nama nama kucing
    private Cage cage;

    public Cage getCage() {
        return cage;
    }

    public void setCage(Cage cage) {
        this.cage = cage;
    }

    String[] voices = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"}; //list suara yang akan di random

    public Cats(String name, int bodylength, boolean pet){ //constructor kucing
        super(name,bodylength,pet); //memanggil constructor animal
        this.cage = new Cage(pet, bodylength);
        this.listnama = listnama;
        listnama.add(name);
    }

    public ArrayList<String> getListnama() {
        return listnama;
    }

    //action
    public void brushthefur(){
        System.out.println("Time to clean "+this.getName()+"'s fur");
        System.out.println("Katty makes a voice: Nyaaan...");
    }

    public void cuddle(){
        int idx = new Random().nextInt(voices.length);
        String random = (voices[idx]);
        System.out.println(this.getName() + " makes a voice: " + random);
    }
}

class Hamsters extends Animal{ //class hamster
    private Cage cage;
    ArrayList<String> listnama = new ArrayList<>();

    public Cage getCage() {
        return cage;
    }

    public void setCage(Cage cage) {
        this.cage = cage;
    }

    public Hamsters(String name, int bodylength, boolean pet){
        super(name,bodylength,pet); //constructor animal
        this.cage = new Cage(pet, bodylength);
        this.listnama = listnama;
        listnama.add(name);
    }

    public ArrayList<String> getListnama() {
        return listnama;
    }

    String[] gnawvoice ={"ngkkrit..", "ngkkrrriiit"};
    int idx = new Random().nextInt(gnawvoice.length);
    String random = (gnawvoice[idx]);

    //action
    public void gnawn(){
        System.out.println("Ronald makes a voice: " + random );
    }
    public void runwheel(){
        System.out.println("Ronald makes a voice: Trrr... Trrr..." );
    }

}

class Parrots extends Animal{ //class parrot
    ArrayList<String> listnama = new ArrayList<>();
    private Cage cage;

    public Cage getCage() {
        return cage;
    }

    public void setCage(Cage cage) {
        this.cage = cage;
    }
    public Parrots(String name, int bodylength,boolean pet){
        super(name,bodylength,pet); //memanggil constructor animal
        this.cage = new Cage(pet, bodylength);
        this.listnama = listnama;
        listnama.add(name);
    }

    public ArrayList<String> getListnama() {
        return listnama;
    }

    public void fly(){
        System.out.println("Parrot " + this.getName() + "flies!");
        System.out.println(this.getName() + " makes a voice: FLYYYYY...");
    }


    //action
    public void imitate(String kata){
        System.out.println(kata.toUpperCase());
    } //meniru input
    public void nothing(){
        System.out.println("HM?");
    }
}

class Lion extends Animal{
    ArrayList<String> listnama = new ArrayList<>();
    private Cage cage;

    public Cage getCage() {
        return cage;
    }

    public void setCage(Cage cage) {
        this.cage = cage;
    }
    public Lion(String name, int bodylength, boolean pet){
        super(name,bodylength,pet);
        this.cage = new Cage(pet, bodylength);
        this.listnama = listnama;
        listnama.add(name);

    }
    //action
    public void hunting(){
        System.out.println("Lion is hunting..");
        System.out.println(this.getName() + " makes a voice: err...!");
    }

    public void brush(){
        System.out.println("Clean the lion's mane..");
        System.out.println(this.getName() + " makes a voice: Hauhhmm!");
    }
    public void disturb(){
        System.out.println(this.getName() + " makes a voice: HAUHHMM!");
    }

    public ArrayList<String> getListnama() {
        return listnama;
    }
}

class Eagle extends Animal{
    ArrayList<String> listnama = new ArrayList<>();
    private Cage cage;

    public Cage getCage() {
        return cage;
    }

    public ArrayList<String> getListnama() {
        return listnama;
    }

    public void setCage(Cage cage) {
        this.cage = cage;
    }
    public Eagle(String name, int bodylength, boolean pet){
        super(name,bodylength,pet);
        this.cage = new Cage(pet, bodylength);
        this.listnama = listnama;
        listnama.add(name);


    }
    //action
    public void fly(){
        System.out.println("Kwaakk...");
    }
}
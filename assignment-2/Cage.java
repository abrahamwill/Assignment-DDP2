//membuat class cage
public class Cage{
    boolean indoor = true;
    String tipe;

    void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    String getTipe() {
        return tipe;
    }

    void setTipe(String tipe) {
        this.tipe = tipe;
    }

    boolean isIndoor() {
        return indoor;
    }

    //menempatkan di tipe kandang yang tepat sesuai panjang dan tipe
    Cage(boolean pet, int length){
        if (pet){
            this.indoor = indoor;
            if (length<45){
                this.tipe = "A";
            }
            else if (length>60){
                this.tipe = "C";
            }
            else{
                this.tipe = "B";
            }
        }
        else{
            this.indoor = false;
            if (length<75){
                this.tipe = "A";
            }
            else if (length>90){
                this.tipe = "C";
            }
            else{
                this.tipe = "B";
            }
        }
    }






}
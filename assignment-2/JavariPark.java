import java.util.Scanner;

public class JavariPark{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        System.out.print("cat : ");

        //input semua jumlah binatang dan data datanya

        //input kucing
        int jumlahkucing = Integer.parseInt(input.nextLine());
        Animal[] kucings = new Animal[jumlahkucing];

        if (jumlahkucing > 0) {
            String[] listkucing = new String[jumlahkucing];

            System.out.println("Provide the information of cat(s):");
            listkucing = input.nextLine().split(",");

            for (int i = 0; i < listkucing.length; i++) {
                String[] tmp = listkucing[i].split("\\|");
                String nama = tmp[0];
                int panjang = Integer.parseInt(tmp[1]);
                Cats kucing = new Cats(nama, panjang, true);
                kucings[i] = kucing;
                Cage kandangkucing = new Cage(kucing.isPet(), panjang);
            }
        }


        //input lion
        System.out.print("lion : ");
        int jumlahsinga = Integer.parseInt(input.nextLine());
        Animal[] singas = new Animal[jumlahsinga];

        if (jumlahsinga > 0) {
            String[] listsinga = new String[jumlahsinga];

            System.out.println("Provide the information of lion(s):");

            listsinga = input.nextLine().split(",");

            for (int i = 0; i < listsinga.length; i++) {
                String[] tmp = listsinga[i].split("\\|");
                String nama = tmp[0];
                int panjang = Integer.parseInt(tmp[1]);
                Lion singa = new Lion(nama, panjang, false);
                singas[i] = singa;
                Cage kandangsinga = new Cage(singa.isPet(), panjang);
            }
        }

        //input eagle
        System.out.print("eagle : ");
        int jumlaheagle = Integer.parseInt(input.nextLine());
        Animal[] eagles = new Animal[jumlaheagle];

        if (jumlaheagle > 0) {
            String[] listeagle = new String[jumlaheagle];
            System.out.println("Provide the information of eagle(s):");

            listeagle = input.nextLine().split(",");

            for (int i = 0; i < listeagle.length; i++) {
                String[] tmp = listeagle[i].split("\\|");
                String nama = tmp[0];
                int panjang = Integer.parseInt(tmp[1]);
                Eagle eagle = new Eagle(nama, panjang, false);
                eagles[i] = eagle;
                Cage sangkareagle = new Cage(eagle.isPet(), panjang);
            }
        }

        //input parrot
        System.out.print("parrot : ");
        int jumlahparrot = Integer.parseInt(input.nextLine());
        Animal[] parrots = new Animal[jumlahparrot];

        if (jumlahparrot > 0) {
            String[] listparrot = new String[jumlahparrot];
            System.out.println("Provide the information of parrot(s):");
            listparrot = input.nextLine().split(",");

            for (int i = 0; i < listparrot.length; i++) {
                String[] tmp = listparrot[i].split("\\|");
                String nama = tmp[0];
                int panjang = Integer.parseInt(tmp[1]);
                Parrots parrot = new Parrots(nama, panjang, false);
                parrots[i] = parrot;
                Cage sangkarparrot = new Cage(parrot.isPet(), panjang);
            }
        }

        //input hamster
        System.out.print("hamster : ");
        int jumlahhamster = Integer.parseInt(input.nextLine());
        Animal[] hamsters = new Animal[jumlahhamster];

        if (jumlahhamster > 0) {
            String[] listhamster = new String[jumlahhamster];
            System.out.println("Provide the information of parrot(s):");
            listhamster = input.nextLine().split(",");

            for (int i = 0; i < listhamster.length; i++) {
                String[] tmp = listhamster[i].split("\\|");
                String nama = tmp[0];
                int panjang = Integer.parseInt(tmp[1]);
                Hamsters hamster = new Hamsters(nama, panjang, false);
                hamsters[i] = hamster;
                Cage kandanghamster = new Cage(hamster.isPet(), panjang);
            }
        }

        System.out.println("Animals have been succesfully recorded!\n\n");

        System.out.println("=============================================");
        System.out.println("Cage arrangement:");

        //print hasil arrange cage dan rearrange cage untuk setiap jenis hewan
        if (jumlahkucing > 0) {
            Arrange.printcage(Arrange.firstarrange(kucings), Arrange.rearrange(Arrange.firstarrange(kucings)));

        }
        if (jumlahsinga > 0) {
            Arrange.printcage(Arrange.firstarrange(singas), Arrange.rearrange(Arrange.firstarrange(singas)));
        }

        if (jumlaheagle > 0) {
            Arrange.printcage(Arrange.firstarrange(eagles), Arrange.rearrange(Arrange.firstarrange(eagles)));
        }

        if (jumlahparrot > 0) {
            Arrange.printcage(Arrange.firstarrange(parrots), Arrange.rearrange(Arrange.firstarrange(parrots)));
        }

        if (jumlahhamster > 0) {
            Arrange.printcage(Arrange.firstarrange(hamsters), Arrange.rearrange(Arrange.firstarrange(hamsters)));
        }

        //print data jumlah sementara
        System.out.println("NUMBER OF ANIMALS:");
        System.out.println("cat:" + kucings.length);
        System.out.println("lion:" + singas.length);
        System.out.println("parrot:" + parrots.length);
        System.out.println("eagle:" + eagles.length);
        System.out.println("hamster:" + hamsters.length);

        //Visit animal

        System.out.println("\n\n=====================================");
        int inp;
        String inpnama;
        while (true) {
            boolean flag = false;
            System.out.println("Which animal you want to visit?\n (1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            inp = input.nextInt();
            if (inp == 1) { //visit kucing
                System.out.print("Mention the name of cat you want to visit: ");
                inpnama = input.next();
                for (Animal x : kucings) {
                    if (x.getName().equals(inpnama)) {
                        System.out.println("You are visiting " + inpnama + " (cat) now, what would you like to do?");
                        System.out.println("1: Brush the fur 2: Cuddle");
                        int perintah = input.nextInt();
                        Cats y = (Cats) x;
                        if (perintah == 1) {
                            y.brushthefur();
                            System.out.println("Back to the office!\n");
                        } else if (perintah == 2) {
                            y.cuddle();
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                        flag = true;
                    }
                }
                if (!flag) { //flag untuk ngecek apakan ada kucing yang bernama sesuai input
                    System.out.println("There is no cat with that name!\nBack to the office!\n");
                }
            } else if (inp == 2) { //visit eagle
                System.out.print("Mention the name of eagle you want to visit: ");
                inpnama = input.next();
                for (Animal x : eagles) {
                    if (x.getName().equals(inpnama)) {
                        System.out.println("You are visiting " + inpnama + " (eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly");
                        int perintah = input.nextInt();
                        Eagle y = (Eagle) x;
                        if (perintah == 1) {
                            y.fly();
                            System.out.println("You hurt!");
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                        flag = true;
                    }
                }
                if (!flag) {
                    System.out.println("There is no eagle with that name!\nBack to the office!\n");
                }

            } else if (inp == 3) { //visit hamster
                System.out.print("Mention the name of hamster you want to visit: ");
                inpnama = input.next();
                for (Animal x : hamsters) {
                    if (x.getName().equals(inpnama)) {
                        System.out.println("You are visiting " + inpnama + " (eagle) now, what would you like to do?");
                        System.out.println("1: See it gnawning 2: Order to run in the hamster wheel");
                        int perintah = input.nextInt();
                        Hamsters y = (Hamsters) x;
                        if (perintah == 1) {
                            y.gnawn();
                            System.out.println("Back to the office!\n");

                        } else if (perintah == 2) {
                            y.runwheel();
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                        flag = true;
                    }
                }
                if (!flag) {
                    System.out.println("There is no hamster with that name!\nBack to the office!\n");
                }
            } else if (inp == 4) { //visit parrot
                System.out.print("Mention the name of parrot you want to visit: ");
                inpnama = input.next();
                for (Animal x : parrots) {
                    if (x.getName().equals(inpnama)) {
                        System.out.println("You are visiting " + inpnama + " (eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly 2: Do conversation");
                        int perintah = input.nextInt();
                        Parrots y = (Parrots) x;
                        if (perintah == 1) {
                            y.fly();
                            System.out.println("Back to the office!\n");

                        } else if (perintah == 2) {
                            System.out.println("You say: ");
                            String conv = input.nextLine();
                            y.imitate(conv);
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("HM?");
                            System.out.println("Back to the office!\n");
                        }
                        flag = true;
                    }
                }
                if (!flag) {
                    System.out.println("There is no parrot with that name!\nBack to the office!\n");
                }
            } else if (inp == 5) {//visit lion
                System.out.print("Mention the name of lion you want to visit: ");
                inpnama = input.next();
                for (Animal x : singas) {
                    if (x.getName().equals(inpnama)) {
                        System.out.println("You are visiting " + inpnama + " (eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly 2: Do conversation");
                        int perintah = input.nextInt();
                        Lion y = (Lion) x;
                        if (perintah == 1) {
                            y.hunting();
                            System.out.println("Back to the office!\n");

                        } else if (perintah == 2) {
                            y.brush();
                            System.out.println("Back to the office!\n");
                        } else if (perintah == 3) {
                            y.disturb();
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                        flag = true;
                    }
                }
                if (!flag) {
                    System.out.println("There is no lion with that name!\nBack to the office!\n");
                }
            } else {
                break;
            }
        }
    }
}

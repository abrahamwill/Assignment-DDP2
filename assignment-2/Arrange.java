import java.util.ArrayList;
import java.util.Arrays;

public class Arrange{

    //Method print hasil arrange dan rearrange
    public static void printcage(ArrayList<ArrayList<Animal>> levels, ArrayList<ArrayList<Animal>> rearrange){
        String tipe;
        int length;
        String print = "";

        if (levels.get(0).get(0).isPet()){
            System.out.println("location: indoor");
        }
        else{
            System.out.println("location: outdoor");
        }

        for (int i = 2; i>=0; i--){
            System.out.print("level " +(i+1)+": ");
            for (Animal x : levels.get(i)){
                String nama = x.getName();

                if (x instanceof Cats){ //mengambil data tiap kucing
                    Cats m = (Cats) x;
                    length = m.getBodylength();
                    tipe = m.getCage().getTipe();
                }
                else if (x instanceof Lion){ //mengambil data tiap singa
                    Lion m = (Lion) x;
                    length = m.getBodylength();
                    tipe = m.getCage().getTipe();

                }
                else if (x instanceof Parrots){ //mengambil data tiap parrot
                    Parrots m = (Parrots) x;
                    length = m.getBodylength();
                    tipe = m.getCage().getTipe();

                }
                else if (x instanceof Eagle){ //mengambil data tiap eagle
                    Eagle m = (Eagle) x;
                    length = m.getBodylength();
                    tipe = m.getCage().getTipe();

                }
                else{
                    Hamsters m = (Hamsters) x;
                    length = m.getBodylength();
                    tipe = m.getCage().getTipe();

                }

                print += (nama+" ("+ length +" - "+ tipe+ "), "); //data dimasukkan ke string

            }

            System.out.println(print); //data print

            print = "";
        }
        System.out.println();

            System.out.println("After rearrangement...");
            for (int i = 2; i>=0; i--){
                System.out.print("level " +(i+1)+": ");
                for (Animal x : rearrange.get(i)){
                    String nama = x.getName();

                    if (x instanceof Cats){
                        Cats m = (Cats) x;
                        length = m.getBodylength();
                        tipe = m.getCage().getTipe();
                    }
                    else if (x instanceof Lion){
                        Lion m = (Lion) x;
                        length = m.getBodylength();
                        tipe = m.getCage().getTipe();

                    }
                    else if (x instanceof Parrots){
                        Parrots m = (Parrots) x;
                        length = m.getBodylength();
                        tipe = m.getCage().getTipe();

                    }
                    else if (x instanceof Eagle){
                        Eagle m = (Eagle) x;
                        length = m.getBodylength();
                        tipe = m.getCage().getTipe();

                    }
                    else{
                        Hamsters m = (Hamsters) x;
                        length = m.getBodylength();
                        tipe = m.getCage().getTipe();

                    }

                    print += (nama+" ("+ length +" - "+ tipe+ "), ");

                }
                System.out.println(print); //print hasil rearrange
                print = "";
            }
        System.out.println();


    }

    //membuat method untuk arrange pertama kali
    public static ArrayList<ArrayList<Animal>> firstarrange(Animal[] animals){
        //membuat arraylist multidimensi baru
        ArrayList<ArrayList<Animal>> levels = new ArrayList<ArrayList<Animal>>();
        ArrayList<Animal> level1 = new ArrayList<Animal>();
        ArrayList<Animal> level2 = new ArrayList<Animal>();
        ArrayList<Animal> level3 = new ArrayList<Animal>();

        ArrayList<Animal> listanimal = new ArrayList<Animal>(Arrays.asList(animals));

        //ada 3 kasus, ketika jumlah animal lebih dari 2, persis 2, dan hanya 1

        if (animals.length>2) {

            int panjang = animals.length / 3;

            for (int j = 0; j < panjang; j++) {
                level1.add(listanimal.get(j));
            }

            for (int j = panjang; j < (2 * panjang); j++) {
                level2.add(listanimal.get(j));
            }

            for (int j = (2 * panjang); j < (listanimal.size()); j++) {
                level3.add(listanimal.get(j));
            }
        }

        else if (animals.length==2){ // jika hanya 2, level 3 dikosongkan
            level1.add(listanimal.get(0));
            level2.add(listanimal.get(1));
        }
        else {
            level1.add(listanimal.get(0)); // jika hanya 1, level 2 dan 3 dikosongkan
        }

        levels.add(level1);
        levels.add(level2);
        levels.add(level3);

        return levels;
    }

    public static ArrayList<ArrayList<Animal>> rearrange(ArrayList<ArrayList<Animal>> animals){
        //membuat arraylist untuk hasil rearrange
        ArrayList<Animal> tmp1 = new ArrayList<Animal>();
        ArrayList<Animal> tmp2 = new ArrayList<Animal>();
        ArrayList<Animal> tmp3 = new ArrayList<Animal>();
        ArrayList<ArrayList<Animal>> hasil = new ArrayList<ArrayList<Animal>>();

        if (animals.get(0).size() > 0) { //mengswap anggota level 1
            for (int x = animals.get(0).size() - 1; x >= 0; x--) {
                tmp1.add(animals.get(0).get(x));
            }
        }
        if (animals.get(1).size() > 0) { //mengswap anggota level 2
            for (int x = animals.get(1).size() - 1; x >= 0; x--) {
                tmp2.add(animals.get(1).get(x));
            }
        }
        if (animals.get(2).size() > 0) { //mengswap angota level 3
            for (int x = animals.get(2).size() - 1; x >= 0; x--) {
                tmp3.add(animals.get(2).get(x));
            }
        }

        //swap posisi level
        hasil.add(tmp3);
        hasil.add(tmp1);
        hasil.add(tmp2);

        return hasil;
   }
}
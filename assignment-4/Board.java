import javax.swing.*;
import java.awt.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import javax.swing.JFrame;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;


public class Board extends JFrame{
    private JFrame frame;
    private JPanel top;
    private JPanel bot;
    private JPanel mid;
    private List<Card> cards;
    private JLabel resetcount;
    private JLabel triescount;
    private JButton resetbutton;
    private JButton exitbutton;
    private int reset = 0;
    private int tries = 0;
    public Card a;
    public Card b;

    private Timer wait;
    private Timer check;


    public Board() {
        int pairs = 18;
        List<Card> cardList = new ArrayList<>();
        List<Integer> cardVals = new ArrayList<>();

        for(int i = 0; i<pairs; i++){
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);

        for(int v : cardVals){
            String filepath = "C:\\Users\\user\\Documents\\Assignment DDP2\\assignment-4\\img\\";
            Card kartu = new Card(resize(new ImageIcon(filepath + "tutles.jpg")),resize(new ImageIcon(filepath + v +".jpg")),v-1);
            kartu.getButton().addActionListener(e -> turn(kartu));
            cardList.add(kartu);
        }
        this.cards = cardList;

        check = new Timer(500, e -> checkMatched());
        check.setRepeats(false);

        Panel();
    }

    public void Panel(){
        frame = new JFrame("Match the Dragon!");
        frame.setLayout(new BorderLayout());
        top = new JPanel();
        top.setLayout(new GridLayout(6,6));


        for (Card x : cards){
            top.add(x.getButton());
        }

        bot = new JPanel(new GridLayout(2,2));
        resetcount = new JLabel("Number of Reset: " + reset);
        triescount = new JLabel("Number of Tries: " + tries);
        resetcount.setHorizontalAlignment(SwingConstants.CENTER);
        triescount.setHorizontalAlignment(SwingConstants.CENTER);
        bot.add(resetcount);
        bot.add(triescount);

        JButton resetbutton = new JButton("Reset");
        JButton exitbutton = new JButton("Exit");
        bot.add(resetbutton);
        bot.add(exitbutton);
        resetbutton.addActionListener(actionPerformed -> reset());
        exitbutton.addActionListener(actionPerformed -> exit());

        frame.add(top, BorderLayout.CENTER);
        frame.add(bot, BorderLayout.SOUTH);

        frame.setPreferredSize(new Dimension(800,800));

        frame.setLocation(400,30);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();

        frame.setVisible(true);

        wait = new Timer(2500, actionPerformed -> revealAll());
        wait.start();
        wait.setRepeats(false);

    }

    private void exit(){
        System.exit(0);
    }

    private void reset(){
        frame.remove(top);
        Collections.shuffle(cards);
        JPanel top = new JPanel(new GridLayout(6,6));
        for (Card x : cards) {
            x.setMatched(false);
            x.release();
            x.getButton().setEnabled(true);
            top.add(x.getButton());
        }

        frame.add(top, BorderLayout.CENTER);
        tries = 0;
        reset++;
        resetcount.setText("Number of Reset: " + reset);
        triescount.setText("Number of Tries: " + tries);
        revealAll();
    }

    public void revealAll(){
        for(Card x : cards){
            x.pressed();
        }
        Timer open = new Timer(3000, actionPerformed -> hideAllCards());
        open.setRepeats(false);
        open.start();
    }

    public void hideAllCards() {
        for (Card card : cards) {
            card.release();
        }
    }

    public void turn(Card selected){
        if(a==null && b==null){
            a = selected;
            a.pressed();
        }

        if(a!=null && a!=selected && b==null){
            b = selected;
            b.pressed();
            check.start();
        }
    }

    public void checkMatched(){
        if (a.getId() == b.getId()){
            a.getButton().setEnabled(false);
            b.getButton().setEnabled(false);
            a.setMatched(true);
            b.setMatched(true);
            if(this.won()){
                JOptionPane.showMessageDialog(this, "You did it! youre da real dragon rida! + \nYou've tried " + tries + " times and reset " + reset +" times");
                System.exit(0);
            }
        }
        else{
            a.release();
            b.release();
            tries++;
            triescount.setText("Number of Tries: " + tries);
        }
        a = null;
        b = null;
    }

    public void sameId(){
        a.getButton().setEnabled(false);
        b.getButton().setEnabled(false);
        a.setMatched(true);
        b.setMatched(true);
    }

    public boolean won(){
        for(Card x : this.cards){
            if (!x.isMatched()){
                return false;
            }
        }
        return true;
    }

    public static ImageIcon resize(ImageIcon icon){
        Image awal = icon.getImage();
        Image akhir = awal.getScaledInstance(133, 116, Image.SCALE_SMOOTH);
        return new ImageIcon(akhir);
    }
}
import javax.swing.*;

public class Card {
    private ImageIcon front;
    private ImageIcon back;
    private final int id;
    private boolean matched = false;
    private final JButton button = new JButton();

    public Card(ImageIcon front, ImageIcon back, int id) {
        this.front = front;
        this.back = back;
        this.id = id;
        this.matched = matched;
        this.button.setIcon(front);
    }

    public void pressed(){
        button.setIcon(back);
    }
    public void release(){
        button.setIcon(front);
    }

    public JButton getButton() {
        return button;
    }

    public ImageIcon getFront() {
        return front;
    }

    public void setFront(ImageIcon front) {
        this.front = front;
    }

    public ImageIcon getBack() {
        return back;
    }

    public void setBack(ImageIcon back) {
        this.back = back;
    }

    public int getId() {
        return id;
    }

    public boolean isMatched() {
        return matched;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

}

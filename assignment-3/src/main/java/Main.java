package main.java.javari;

import main.java.javari.reader.Reader;
import javari.reader.CsvReader;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.print("... Opening default section database from data.");

        String defpath = "C:\\Users\\user\\Documents\\Assignment DDP2\\assignment-3\\data";


        while(true){
            try{
                Path attractionspath = Paths.get(defpath + "\\animals_attractions.csv");
                Path categoriespath = Paths.get(defpath + "\\animals_categories.csv");
                Path recordsfile = Paths.get(defpath + "\\animal_records.csv");

                CsvReader attractions = new Reader(attractionspath);
                CsvReader categories = new Reader(categoriespath);
                CsvReader records = new Reader(recordsfile);


            }
        }
    }

}


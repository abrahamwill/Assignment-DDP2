package main.java.javari.animal;

import javari.animal.Animal;
import javari.animal.Gender;
import javari.animal.Condition;

public class Reptiles extends Animal{

    private boolean wild;

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                  double weight,String kind, Condition condition) {
        super(id, type, name, gender, length, weight, condition);

        if (kind.equals("wild")) {
            this.wild = true;
        } else {
            this.wild = false;
        }
    }

    protected boolean specificCondition(){
        return !(wild);

    }

}

public class Snake extends Reptiles{

    public Snake(Integer id, String type, String name, Gender gender, double length,
                    double weight,String kind, Condition condition) {
        super(id, type, name, gender, length, weight, kind,condition);

}

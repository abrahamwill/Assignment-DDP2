package main.java.javari.animal;

import javari.animal.Animal;
import javari.animal.Gender;
import javari.animal.Condition;

public class Aves extends Animal {

    private boolean layingEggs = false;

    public Aves (Integer id, String type, String name, Gender gender, double length,
                 double weight,String layingEggs, Condition condition){
        super(id, type, name, gender, length, weight, condition);

        if (layingEggs.equals("laying")){
            this.layingEggs = true;
        }
        else{
            this.layingEggs = false;
        }


    }

    protected boolean specificCondition(){
        return !(this.layingEggs);
    }


    public boolean isLayingEggs() {
        return layingEggs;
    }

    public void setLayingEggs(boolean layingEggs) {
        this.layingEggs = layingEggs;
    }
}

public class Eagle extends Aves{
    public Eagle(Integer id, String type, String name, Gender gender, double length,
                 double weight, String layingEggs, Condition condition) {
        super(id, type, name, gender, length, weight,layingEggs, condition);
    }


}

public class Parrot extends Aves{
    public Parrot(Integer id, String type, String name, Gender gender, double length,
                 double weight, String layingEggs, Condition condition) {
        super(id, type, name, gender, length, weight, layingEggs, condition);
    }

}
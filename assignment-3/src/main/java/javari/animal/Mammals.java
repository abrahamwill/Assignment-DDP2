package main.java.javari.animal;

import javari.animal.Animal;
import javari.animal.Gender;
import javari.animal.Condition;



public class Mammals extends Animal{
    private boolean isPregnant;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);

        if (pregnant.equals("pregnant")) {
            this.isPregnant = true;
        } else {
            this.isPregnant = false;
        }
    }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }

    protected boolean specificCondition(){
        return !(this.isPregnant);
    }

}

public class Cat extends Mammals{
    public Cat(Integer id, String type, String name, Gender gender, double length,
                   double weight,String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, pregnant, condition);
}

public class Hamster extends Mammals{
    public Hamster(Integer id, String type, String name, Gender gender, double length,
                   double weight,  String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, pregnant, condition);


}

public class Lion extends Mammals{
    public Lion(Integer id, String type, String name, Gender gender, double length,
                   double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, pregnant, condition);

}

public class Whale extends Mammals{
    public Whale(Integer id, String type, String name, Gender gender, double length,
                   double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, pregnant, condition);
}

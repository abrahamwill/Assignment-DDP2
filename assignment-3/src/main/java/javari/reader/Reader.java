package main.java.javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import javari.reader.CsvReader;


public class Reader extends CsvReader{

    public Reader(Path file) throws IOException{
        super(file);
    }

    @Override
    public long countValidRecords() {
        return 0;
    }

    @Override
    public long countInvalidRecords() {
        return 0;
    }
}

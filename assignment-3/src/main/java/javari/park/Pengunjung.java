package main.java.javari.park;

import javari.animal.Animal;
import javari.park.SelectedAttraction;
import javari.park.Registration;

import java.util.ArrayList;
import java.util.List;

public class Pengunjung implements Registration{

    private static int id = 0;
    private List<SelectedAttraction> atraksi = new List<SelectedAttraction>();
    private String name;

    public Pengunjung(String name) {
        this.name = name;
        id+=1;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Pengunjung.id = id;
    }

    public List<SelectedAttraction> getAtraksi() {
        return atraksi;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

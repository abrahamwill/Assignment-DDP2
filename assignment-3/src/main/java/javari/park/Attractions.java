package main.java.javari.park;

import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;
import javari.park.SelectedAttraction;

public class Attractions implements SelectedAttraction {
    private List<Animal> performers = new ArrayList<>();
    private String type;
    private String name;
    private List<Attractions>  listattractions = new ArrayList<>();

    public Attractions(String name, String type){
        this.name = name;
        this.type = type;
        listattractions.add(this);

    }

    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        else{
            return false;
        }
    }

    public List<Animal> getPerformers() {
        return performers;
    }

    public void setPerformers(List<Animal> performers) {
        this.performers = performers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Attractions> getListattractions() {
        return listattractions;
    }

    public void setListattractions(List<Attractions> listattractions) {
        this.listattractions = listattractions;
    }
}

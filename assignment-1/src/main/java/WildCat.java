public class WildCat {

    // TODO Complete me!
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getLength() {
        return length;
    }

    public double computeMassIndex() {
        // TODO Complete me!
        double massindex = this.weight / Math.pow((length/100.0), 2);
        return massindex;
    }
}

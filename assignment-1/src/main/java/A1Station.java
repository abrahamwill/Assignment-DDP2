import java.util.Scanner;
import java.util.ArrayList;

public class  A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    public static ArrayList<TrainCar> listkereta = new ArrayList<TrainCar>();

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();

        double beratsementara = 0;

        for (int i = 0; i < number ; i++) {
            String inp = input.next();
            String[] tmp = inp.split(",");

            String name = tmp[0];
            double weight = Double.parseDouble(tmp[1]);
            double length = Double.parseDouble(tmp[2]);

            WildCat cat = new WildCat(name, weight, length);


            TrainCar kereta = new TrainCar(cat);

            listkereta.add(kereta);
            beratsementara += listkereta.get(listkereta.size()-1).computeTotalWeight();

            if (beratsementara > THRESHOLD){
                depart();
                beratsementara = 0;
                listkereta.clear();
            }

        }
        if (beratsementara != 0){
            depart();
        }
    }

    public static void depart(){
        for (int i = (listkereta.size()-1); i>0; i--){
            listkereta.get(i).next = listkereta.get(i-1);
        }

        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");

        listkereta.get(listkereta.size()-1).printCar();

        double avgmass = listkereta.get(listkereta.size()-1).computeTotalMassIndex() / listkereta.size();

        System.out.println("Average mass index of all cats: " + avgmass);
        String category ="";


        if (avgmass<18.5){
            category += "underweight";
        }
        else if (avgmass>=18.5 && avgmass <25){
            category += "normal";
        }
        else if (avgmass>=
                25 && avgmass<30){
            category +="overweight";
        }
        else{
            category +="obese";
        }

        System.out.println("In average, they are " + category);
    }



        // TODO Complete me!





}